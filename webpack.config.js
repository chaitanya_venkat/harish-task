var webpack = require('webpack');
var path = require('path');

var APP_DIR = path.resolve(__dirname + 'js/');
var BUILD_DIR = path.resolve(__dirname, 'dist/');

module.exports = {
	entry: './js/app.js',
	output: {
		path: BUILD_DIR,
		publicPath: '/dist',
		filename: 'app_bundle.js'
	},
	resolve: {
    	extensions: ['.js']
  	},
	module: {
		loaders: [
			{
				test: /\.s?css$/,
				use: ['style-loader', 'css-loader', 'sass-loader'],
			},
			{
				test: /\.(js|jsx)$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
		        query: {
		          presets: ['es2015', 'stage-3']
		        }
			},
			{
				test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/,
				loader: 'file-loader'
			}
		]
	},
	devServer: {
		compress: true
	}
}
