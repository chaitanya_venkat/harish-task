angular.module('musicAppService', [])
.factory('GetComments', function($http) {
  return $http({method: 'GET', url: 'https://evening-badlands-69939.herokuapp.com/api/comments/'});
})
.factory('GetBlogs', function($http) {
  return $http({method: 'GET', url: 'https://evening-badlands-69939.herokuapp.com/api/posts/'});
})
.factory('BlogDetails', function($http) {
  var BlogDetailsAPI = {};
  BlogDetailsAPI.getDetails = function (url) {
    return $http.get(url);
  }
  return BlogDetailsAPI;
});
