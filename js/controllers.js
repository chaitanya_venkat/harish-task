angular.module('musicAppController', [])
.controller('HomePageController', function($scope, $state, $window, GetBlogs) {
  $scope.blogs = [];
  GetBlogs.then(function success(resp) {
    $scope.blogs = resp.data;
  })
})
.controller('BlogViewController', function($scope, $stateParams, $http, Music, Search, BlogDetails) {
  $scope.blog = {};
  var post_url = $stateParams.blogurl;
  BlogDetails.getDetails(post_url)
  .then(function succesCallBack(response) {
    $scope.blog = response.data;
  },
  function errorCallBack(err) {
    console.log(err);
  })
});
