angular.module('blogApp', ['ui.router', 'ngResource', 'musicAppController', 'musicAppService'])
.config(function($stateProvider) {
  $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'partials/home.html',
    controller: 'HomePageController'
  }).state('viewBlog', {
    url: '/blog/:blogurl',
    templateUrl: 'partials/blog-view.html',
    controller: 'BlogViewController'
  }).state('newMusic', {
    url: '/music/new',
    templateUrl: 'partials/music-add.html',
    controller: 'MusicCreateController'
  }).state('editBlog', {
    url: '/blog/:slug/edit',
    templateUrl: 'partials/blog-edit.html',
    controller: 'BlogEditController'
  });
})
.run(function($state) {
  $state.go('home');
});

function goBack() {
    window.history.back();
}
